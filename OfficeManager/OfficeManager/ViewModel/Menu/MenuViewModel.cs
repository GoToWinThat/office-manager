﻿using OfficeManager.ViewModel.BaseViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OfficeManager.ViewModel.MenuViewModel
{
    public class MenuViewModel:AViewModel
    {
        public MenuViewModel()
        {

        }
        private int currentContent = (int)Contents.PDF;
        public int CurrentContent
        {
            get { return currentContent; }
            set
            {
                currentContent = value;
                OnPropertyChanged();
            }
        }


        public ICommand PDFButtonCommand
        {
            get
            {
                return new RelayCommand(PDFButtonCommandExecute);
            }
        }
        private void PDFButtonCommandExecute(object param =null)
        {
            CurrentContent = (int)Contents.PDF;
        }
        public ICommand WordButtonCommand
        {
            get
            {
                return new RelayCommand(WordButtonCommandExecute);
            }
        }
        private void WordButtonCommandExecute(object param = null)
        {
            CurrentContent = (int)Contents.Word;
        }
        public ICommand ExcelButtonCommand
        {
            get
            {
                return new RelayCommand(ExcelButtonCommandExecute);
            }
        }
        private void ExcelButtonCommandExecute(object param = null)
        {
            CurrentContent = (int)Contents.Excel;
        }
        public ICommand NoteButtonCommand
        {
            get
            {
                return new RelayCommand(NoteButtonCommandExecute);
            }
        }
        private void NoteButtonCommandExecute(object param = null)
        {
            CurrentContent = (int)Contents.Note;
        }



    }
    public enum Contents
    {
        PDF = 0,
        Word = 1,
        Excel = 2,
        Note = 3
    }


   

}