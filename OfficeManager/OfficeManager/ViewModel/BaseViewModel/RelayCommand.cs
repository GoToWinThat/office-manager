﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OfficeManager.ViewModel.BaseViewModel
{
    public class RelayCommand : ICommand
    {
        private Action<object> executeAction;
        public event EventHandler CanExecuteChanged = (sender, e) => { };

        public RelayCommand(Action<object> action)
        {
            this.executeAction = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }


        public void Execute(object parameter)
        {
            this.executeAction(parameter);
        }

    }
}
