﻿using OfficeManager.Model.BusinessCard;
using OfficeManager.Model.BusinessCard.BasicCard;
using OfficeManager.Model.OpenDataSet;
using OfficeManager.ViewModel.BaseViewModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OfficeManager.ViewModel.PDFViewModel.BusinessCard
{
    public class BasicCardOptionViewModel : AViewModel
    {
        public  BasicCardOptionViewModel(){ }

        #region Fonts  

        private List<string> fonts = new List<string> { "Times New Roman", "Calibri", "Tahoma", "Rockwell", "Arial"};
        public List<string> Fonts
        {
            get { return fonts; }
            set 
            { 
                fonts = value;
                OnPropertyChanged();
            }
        }

        private string selectedFont;
        public string SelectedFont
        {
            get { return selectedFont; }
            set
            {
                selectedFont = value;
                OnPropertyChanged();
            }
        }
        #endregion Fonts

        #region FontSize
        private List<double> fontSizes = Enumerable.Range(1, 30).Select(n => Convert.ToDouble(n)).ToList();
        public List<double> FontSizes
        {
            get { return fontSizes; }
            set
            {
                fontSizes = value;
                OnPropertyChanged();
            }
        }

        private double selectedSize = 20;
        public double SelectedSize
        {
            get { return selectedSize; }
            set
            {
                selectedSize = value;
                OnPropertyChanged();
            }
        }
        #endregion FontSize

        #region FontColor

        private List<string> fontColors = new List<string> { "Black", "Gray", "Red", "Blue", "Green", "Purple","Yellow", "Orange" };
        public List<string> FontColors
        {
            get { return fontColors; }
            set
            {
                fontColors = value;
                OnPropertyChanged();
            }
        }

        private string selectedColor;
        public string SelectedColor
        {
            get { return selectedColor; }
            set
            {
                selectedColor = value;
                OnPropertyChanged();
            }
        }
        #endregion Fonts

        #region CardsOnPage

        private List<int> cardsOnPage = Enumerable.Range(1, 8).Select(n => Convert.ToInt32(n)).ToList();
        public List<int> CardsOnPage
        {
            get { return cardsOnPage; }
            set
            {
                cardsOnPage = value;
                OnPropertyChanged();
            }
        }

        private int selectedNumber;
        public int SelectedNumber
        {
            get { return selectedNumber; }
            set
            {
                selectedNumber = value;
                OnPropertyChanged();
            }
        }

        #endregion CardsOnPage

        #region Print
        public ICommand DoublePrintCommand
        {
            get
            {
                return new RelayCommand(DoublePrintCommandExecute);
            }
        }
        private bool IsDoublePrint = true;
        private void DoublePrintCommandExecute(object param)
        {
            if (param == null) { }
            else if ((string)param == "Tak")
            {
                IsDoublePrint = true;
            }
            else if ((string)param == "Nie")
            {
                IsDoublePrint = false;
            }
        }


        public ICommand SplitterPrintCommand
        {
            get
            {
                return new RelayCommand(SplitterPrintCommandExecute);
            }
        }
        private bool IsSplitting = true;
        private void SplitterPrintCommandExecute(object param)
        {
            if (param == null) { }
            else if ((string)param == "Tak")
            {
                IsSplitting = true;
            }
            else if ((string)param == "Nie")
            {
                IsSplitting = false;
            }
        }
        #endregion Druk

        #region Border
        public ICommand BorderCommand
        {
            get
            {
                return new RelayCommand(BorderCommandExecute);
            }
        }
        private bool IsBorderEnable = true;
        private void BorderCommandExecute(object param)
        {
            if (param == null) { }
            else if ((string)param == "Tak")
            {
                IsBorderEnable = true;
            }
            else if ((string)param == "Nie")
            {
                IsBorderEnable = false;
            }
        }

        public ICommand SideBorderCommand
        {
            get
            {
                return new RelayCommand(SideBorderCommandExecute);
            }
        }
        private bool IsSideborderEnable = true;
        private void SideBorderCommandExecute(object param)
        {
            if (param == null) { }
            else if ((string)param == "Tak")
            {
                IsSideborderEnable = true;
            }
            else if ((string)param == "Nie")
            {
                IsSideborderEnable = false;
            }
        }
        #endregion

        #region DataSet

        private string dataSetPath;
        public string DataSetPath
        {
            get { return dataSetPath; }
            set
            {
                dataSetPath = value;
                OnPropertyChanged();
            }
        }

        private List<string> dataSet;
        public List<string> DataSet
        {
            get { return dataSet; }
            set
            {
                dataSet = value;
                OnPropertyChanged();
            }
        }

        public ICommand ChooseDataSetCommand
        {
            get
            {
                return new RelayCommand(ChooseDataSetCommandExecute);
            }
        }

        private void ChooseDataSetCommandExecute(object parameter = null)
        {
            OpenDialogAsync();
        }

        private void OpenDialogAsync()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "DataSet",
                DefaultExt = ".txt"
            };
            if (dialog.ShowDialog() == true)
            {
                Task.Run(() => ReadDataAsync(dialog.FileName));
            }
        }
        private async Task ReadDataAsync(string fileName)
        {
            DataSet = await FacadeFileOpen.OpenAndReadFileAsync(fileName);
            if(DataSet==null || DataSet.Count==0)
            {
                ErrorVisibility = "Visible";
                ErrorMessage = "!!! Nie udało się pobrać danych !!!";
            }
            else 
            {
                DataSetPath = fileName;
                isDataSetLoaded = true;
                ErrorVisibility = "Hidden";
                EnableButton();
            }
        }

        #endregion DataSet

        #region Image

        private string imagePath;
        public string ImagePath
        {
            get { return imagePath; }
            set
            {
                imagePath = value;
                OnPropertyChanged();
            }
        }

        public ICommand ChoosePictureCommand
        {
            get
            {
                return new RelayCommand(ChoosePictureCommandExecute);
            }
        }

        private void ChoosePictureCommandExecute(object parameter = null)
        {
            OpenDialog();
        }

        private void OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "Picture",
                DefaultExt = ".jpg"
            };
            if (dialog.ShowDialog() == true)
            {
                FileInfo fileInfo = new FileInfo(dialog.FileName);
                if (fileInfo.Extension != ".png" && fileInfo.Extension != ".jpg" && fileInfo.Extension != ".bmp" && fileInfo.Extension != ".gif")
                {
                    ErrorMessage = "!!! Zły format (png,jpg,bmp,gif) !!!";
                    ErrorVisibility = "Visible";
                    return;
                }
                else if(IsImageCorrupt(dialog.FileName))
                {
                    ImagePath = dialog.FileName;
                    isImageLoaded = true;
                    ErrorVisibility = "Hidden";
                    EnableButton();
                }
                else
                {
                    ErrorMessage = "!!! Plik jest uszkodzony !!!";
                    ErrorVisibility = "Visible";
                    return;
                }
            }
        }
        public bool IsImageCorrupt(string filename)
        {
            try
            {
                using (var bmp = new Bitmap(filename))
                {}
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion Image

        #region CreatingCards
        public ICommand CreateCardsCommand
        {
            get
            {
                return new RelayCommand(CreateCardsCommandExecute);
            }
        }
        Task creating;
        private void CreateCardsCommandExecute(object param)
        {
            if (creating==null)
            {
                creating = new Task(async () => await CreateCardsDocument());
                creating.Start();
            }
            else if (creating.IsCompleted==true)
            {
                creating = new Task(async () => await CreateCardsDocument());
                creating.Start();
            }
        }
        private async Task CreateCardsDocument()
        {     
            await BusinessCardCreator.CreateBasicCard(builder => builder.SetCardsOnPage(selectedNumber)
                    .SetDoublePrint(IsDoublePrint)
                    .SetSplitingLines(IsSplitting)
                    .SetFont(selectedFont, selectedSize)
                    .SetFontColor(selectedColor)
                    .SetBorder(IsBorderEnable)
                    .SetSideBorder(IsSideborderEnable),
                    dataSet,
                    imagePath);
        }

        #endregion CreatingCards

        #region GenerateCardButton

        private void EnableButton()
        {
            if (isImageLoaded == true && isDataSetLoaded == true)
            {
                IsCreateCardsButtonEnable = true;
            }
        }
       
        private bool isImageLoaded=false;
        private bool isDataSetLoaded = false;

        private bool isCreateCardsButtonEnable = false;
        public bool IsCreateCardsButtonEnable
        {
            get { return isCreateCardsButtonEnable; }
            set
            {
                isCreateCardsButtonEnable = value;
                OnPropertyChanged();
            }
        }

        #endregion GenerateCardButton

        #region Error
        private string errorVisibility = "Hidden";
        public string ErrorVisibility
        {
            get { return errorVisibility; }
            set
            {
                errorVisibility = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;

        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        #endregion Error
    }
}
