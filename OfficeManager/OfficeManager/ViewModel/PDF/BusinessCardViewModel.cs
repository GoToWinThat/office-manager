﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using OfficeManager.ViewModel.BaseViewModel;


namespace OfficeManager.ViewModel.PDFViewModel.BusinessCard
{
    class BusinessCardViewModel : AViewModel
    {
        public BusinessCardViewModel()
        {

        }

        #region CurrentContent
        public enum Contents
        {
            Menu = 0,
            BasicCard = 1,
            LogoCard = 2,
            SidePhotoCard = 3,
            ThemeColorCard = 4
        }

        private int currentContent = (int)Contents.Menu;
        public int CurrentContent
        {
            get { return currentContent; }
            set
            {
                currentContent = value;
                OnPropertyChanged();
            }
        }
        #endregion CurrentContent

        #region Visibility
        private string prevVisibility = "Hidden";

        public string PrevVisibility
        {
            get { return prevVisibility; }
            set
            {
                prevVisibility = value;
                OnPropertyChanged();
            }
        }

        private string nextVisibility = "Hidden";

        public string NextVisibility
        {
            get { return nextVisibility; }
            set
            {
                nextVisibility = value;
                OnPropertyChanged();
            }
        }

        #endregion Visibility

        #region CardCommands
        public ICommand BasicCardCommand
        {
            get
            {
                return new RelayCommand(BasicCardCommandExecute);
            }
        }

        private void BasicCardCommandExecute(object parameter = null)
        {
            CurrentContent = (int)Contents.BasicCard;
            PrevVisibility = "Visible";
        }

        public ICommand LogoCardCommand
        {
            get
            {
                return new RelayCommand(LogoCardCommandExecute);
            }
        }

        private void LogoCardCommandExecute(object parameter = null)
        {
            CurrentContent = (int)Contents.LogoCard;
            PrevVisibility = "Visible";
        }

        public ICommand SidePhotoCardCommand
        {
            get
            {
                return new RelayCommand(SidePhotoCardCommandExecute);
            }
        }

        private void SidePhotoCardCommandExecute(object parameter = null)
        {
            CurrentContent = (int)Contents.SidePhotoCard;
            PrevVisibility = "Visible";
        }

        public ICommand ThemeColorCardCommand
        {
            get
            {
                return new RelayCommand(ThemeColorCardCommandExecute);
            }
        }

        private void ThemeColorCardCommandExecute(object parameter = null)
        {
            CurrentContent = (int)Contents.ThemeColorCard;
            PrevVisibility = "Visible";
        }

        #endregion CardCommands

        #region MenuNavbarCommands

        public ICommand NextCommand
        {
            get
            {
                return new RelayCommand(NextCommandExecute);
            }
        }

        private void NextCommandExecute(object parameter = null)
        {
            //No included in BasicCardVersion - no view to go
        }

        public ICommand PrevCommand
        {
            get
            {
                return new RelayCommand(PrevCommandExecute);
            }
        }

        private void PrevCommandExecute(object parameter = null)
        {
            if(CurrentContent!=0)
            {
                CurrentContent = (int)Contents.Menu;
                PrevVisibility = "Hidden";
            }
        }

        #endregion MenuNavbarCommands

    }
}
