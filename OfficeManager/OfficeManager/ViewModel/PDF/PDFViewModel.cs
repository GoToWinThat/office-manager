﻿using OfficeManager.ViewModel.BaseViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OfficeManager.ViewModel.PDFViewModel
{
    public class PDFViewModel : AViewModel
    {
        public PDFViewModel()
        {

        }
        private int currentContent = (int)Contents.BusinessCard;
        public int CurrentContent
        {
            get { return currentContent; }
            set
            {
                currentContent = value;
                OnPropertyChanged();
            }
        }


        public ICommand BusinessCardButtonCommand
        {
            get
            {
                return new RelayCommand(BusinessCardButtonCommandExecute);
            }
        }
        private void BusinessCardButtonCommandExecute(object param = null)
        {
            CurrentContent = (int)Contents.BusinessCard;
        }
        public ICommand MergePDFButtonCommand
        {
            get
            {
                return new RelayCommand(MergePDFButtonCommandExecute);
            }
        }
        private void MergePDFButtonCommandExecute(object param = null)
        {
            CurrentContent = (int)Contents.MergePDF;
        }
    }
    public enum Contents
    {
        BusinessCard = 0,
        MergePDF = 1,
    }
}