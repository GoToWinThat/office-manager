﻿using CsvHelper;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    class CsvFileReader : FileDataFilter, IFileOpen
    {
        public bool CanOpen(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Extension == ".csv")
                return true;
            else
                return false;
        }
        
        public async Task<List<string>> ReadAsync(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            using (var csv = new CsvReader(reader, CultureInfo.CurrentCulture))
            {
                csv.Configuration.HasHeaderRecord = false;
                var records = new List<string>();
                while (await csv.ReadAsync())
                {
                    try
                    {
                        var record = new Person
                        {
                            Name = csv.GetField(0),
                            Surname = csv.GetField(1)
                        };
                        records.Add(record.Name + " " + record.Surname);
                    }
                    catch{ }
                }
                return ClearStringCollection(records);
            }
        }
    }
}
