﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    class JsonFileReader : FileDataFilter, IFileOpen
    {
        public bool CanOpen(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Extension == ".json")
                return true;
            else
                return false;
        }

        public async Task<List<string>> ReadAsync(string fileName)
        {
            using (StreamReader file = File.OpenText(fileName))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                List<string> records = new List<string>();
                try
                {
                    JToken jToken = await JToken.ReadFromAsync(reader);
                    var recordsArray = jToken.ToObject<Person[]>();
                    records = recordsArray.Select(x => x.Name + " " + x.Surname).ToList();
                }
                catch { }

                return ClearStringCollection(records);
            }
        }
    }
}
