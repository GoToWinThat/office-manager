﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    public class Person
    {
        [Index(0)]
        public string Name { get; set; }
        [Index(1)]
        public string Surname { get; set; }
    }
}
