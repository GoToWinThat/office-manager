﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    public class FileDataFilter
    {
        // Result string contains only letters and number and "-" for double surnames
        // Final string is trimmed and clear from additional whitespaces beetwen first and second name
        protected  List<string> ClearStringCollection(List<string> collection)
        {
            Regex regex = new Regex("[^a-z0-9- ]+", RegexOptions.IgnoreCase);

            for (int i = 0; i < collection.Count; i++)
            {
                collection[i] = RemoveExcessiveWhitespace(regex.Replace(collection[i], String.Empty).Trim());
            }

            collection.RemoveAll(s => string.IsNullOrWhiteSpace(s));

            return collection;
        }
        protected string RemoveExcessiveWhitespace(string value)
        {
            if (value == null) { return null; }

            var builder = new StringBuilder();
            var ignoreWhitespace = false;
            foreach (var c in value)
            {
                if (!ignoreWhitespace || c != ' ')
                {
                    builder.Append(c);
                }
                ignoreWhitespace = c == ' ';
            }
            return builder.ToString();
        }
    }
}
