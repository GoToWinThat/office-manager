﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    class TxtFileReader : FileDataFilter, IFileOpen
    {
        public bool CanOpen(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Extension == ".txt")
                return true;
            else
                return false;
        }

        public async Task<List<string>> ReadAsync(string fileName)
        {
            using (var reader = File.OpenText(fileName))
            {
                List<string> records = new List<string>();
                try
                {
                    var fileText = await reader.ReadToEndAsync();

                    records= fileText.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();


                }
                catch { }
                return ClearStringCollection(records);
            }
        }
    }
}
