﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    public static class FacadeFileOpen
    {
        public static async Task<List<string>> OpenAndReadFileAsync(string fileName)
        {
            return await OpenAndReadFileAsync(FindFileOpeners(), fileName);
        }
        private static List<IFileOpen> FindFileOpeners()
        {
            List<IFileOpen> FileOpeners = new List<IFileOpen>();
            foreach (var t in typeof(IFileOpen).Assembly.GetTypes())
            {
                if (typeof(IFileOpen).IsAssignableFrom(t) && !t.IsInterface)
                {
                    FileOpeners.Add((IFileOpen)Activator.CreateInstance(t));
                }
            }
            return FileOpeners;
        }
        private static async Task<List<string>> OpenAndReadFileAsync(List<IFileOpen> FileOpeners,string fileName)
        {
            foreach (var f in FileOpeners)
            {
                if(f.CanOpen(fileName) == true)
                {
                    return await f.ReadAsync(fileName);
                }
            }
            return null;
        }
    }
}
