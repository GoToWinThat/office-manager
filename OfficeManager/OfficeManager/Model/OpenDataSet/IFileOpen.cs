﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.OpenDataSet
{
    public interface IFileOpen
    {
        Task<List<string>> ReadAsync(string fileName);
        bool CanOpen(string fileName);
    }
}
