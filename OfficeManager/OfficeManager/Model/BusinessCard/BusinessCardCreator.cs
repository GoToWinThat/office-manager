﻿using OfficeManager.Model.BusinessCard.BasicCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.BusinessCard
{
    public static class BusinessCardCreator
    {
        public static async Task CreateBasicCard( Action<BasicCardSettingsBuilder> builder, List<string> dataSet, string imagePath)
        {
            BasicCardSettings basicCardSettings = new BasicCardSettings();
            builder(new BasicCardSettingsBuilder(basicCardSettings));
            await BasicCardCreator.CreateCardDocument(basicCardSettings, dataSet, imagePath);
        }
    }
}
