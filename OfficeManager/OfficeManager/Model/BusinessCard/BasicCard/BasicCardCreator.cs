﻿using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace OfficeManager.Model.BusinessCard.BasicCard
{
    public static class BasicCardCreator
    {
        private static BasicCardSettings Settings;
        private static XImage Image;
        private static List<string> DataSet;
        private static int DataSetIterator = 0;
        private static string imagePath;
        private static PdfDocument document;
        private static XGraphics gfx;
        private static XSize textSize;
        private static int pageCount;
        private static string fileName = "BusinessCards";
        private static readonly XPen pen = new XPen(XColors.Gray, 0.05);
        public static async Task CreateCardDocument(BasicCardSettings settings, List<string> dataSet, string imagePath)
        {
            SaveProperties(settings, dataSet, imagePath);
            await Task.Run(() => CreateDocument());
        }
        private static void SaveProperties(BasicCardSettings settings, List<string> dataSet, string imagePath)
        {
            Settings = settings;
            DataSet = dataSet;
            BasicCardCreator.imagePath = imagePath;
            document = new PdfDocument();
            Image = XImage.FromFile(BasicCardCreator.imagePath);
            imageHeight = Image.PixelHeight;
            imageWidth = Image.PixelWidth;
        }
        private static void CreateDocument()
        {
            CheckSchema();
            if (Settings.CardOnPage != 0)
            {
                CalculatePageCount();
                AddAllPages();
                SaveDocument();
                ResetProperties();
                DisposeAll();
            }
        }
        private static void CalculatePageCount()
        {
            pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(DataSet.Count) / Settings.CardOnPage));
        }
        private static void AddAllPages()
        {
            if (Settings.DoublePrint == true)
            {
                for (int i = 0; i < pageCount; i++)
                {
                    AddNewPage(document.AddPage());
                    AddNewEmptyPage(document.AddPage());
                }
            }
            else
            {
                for (int i = 0; i < pageCount; i++)
                {
                    AddNewPage(document.AddPage());
                }
            }
        }
        private static void SaveDocument()
        {
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "Document",
                DefaultExt = ".pdf",
                Filter = "PDF documents (.pdf)|*.pdf"
            };

            if (dialog.ShowDialog() == true)
            {
                fileName = dialog.FileName;
                try
                {
                    document.Save(fileName);
                }
                catch { }
            }
        }
        private static void AddNewEmptyPage(PdfPage page)
        {
            page.Size = PageSize.A4;
        }

        //This properties are static because when we have only one column we are starting from left side right away
        //In two column case max width with space its 298 Xunits from our calculation in CheckWidthFit
        private static readonly int spaceFromLefttSideFirstColumn = 4;
        private static readonly  int spaceFromLeftSideSecondColumn = 301;

        private static void AddNewPage(PdfPage page)
        {
            page.Size = PageSize.A4;
            gfx = XGraphics.FromPdfPage(page);

            DrawCardColumn(firstColumnEntryPoints, spaceFromLefttSideFirstColumn);
            DrawCardColumn(secondColumnEntryPoints, spaceFromLeftSideSecondColumn);

            if (Settings.SplitterLines)
            {
                DrawSplitLines();
            }
        }

        private static void DrawCardColumn(List<double> ColumnEntryPoints, int widthEntryPoint)
        {
            if (DataSetIterator >= DataSet.Count)
            {
                return;
            }
            for (int i = 0; i < ColumnEntryPoints.Count; i++)
            {
                try
                {
                    gfx.DrawImage(Image, widthEntryPoint, ColumnEntryPoints[i]);
                }
                catch (Exception)
                {
                    Image = XImage.FromFile(imagePath);
                    gfx.DrawImage(Image, widthEntryPoint, ColumnEntryPoints[i]);
                }
                if (DataSetIterator >= DataSet.Count)
                {
                    break;
                }
                if (i % 2 == 0)
                {

                    textSize = gfx.MeasureString(DataSet[DataSetIterator], Settings.Font);

                    //Calculate Entry Point for text
                    //PDFsharp is drawing string in a "box", entry point is left bottom
                    double x = widthEntryPoint + (imageWidthXUnit - textSize.Width) / 2;
                    double y = ColumnEntryPoints[i] + 2 * imageHeightXUnit + textSize.Height / 3;

                    gfx.DrawString(DataSet[DataSetIterator], Settings.Font, Settings.FontColor, x, y);

                    if (Settings.Border)
                    {
                        DrawBorder(widthEntryPoint, ColumnEntryPoints[i], imageWidthXUnit+0.2, imageHeightXUnit * CardHeightMultiplier);
                    }
                    else if (Settings.SideBorder)
                    {
                        DrawSideBorder(
                            new Point(widthEntryPoint, ColumnEntryPoints[i] +  imageHeightXUnit),
                            new Point(widthEntryPoint, ColumnEntryPoints[i] + 3 * imageHeightXUnit));
                        DrawSideBorder(
                            new Point(widthEntryPoint + imageWidthXUnit, ColumnEntryPoints[i] + imageHeightXUnit),
                            new Point(widthEntryPoint + imageWidthXUnit, ColumnEntryPoints[i] + 3 * imageHeightXUnit));
                    }                
                    DataSetIterator++;
                }
            }
        }

        private static void DrawSplitLines()
        {
            if (secondColumnEntryPoints.Count() > 0)
            {
                gfx.DrawLine(pen, 297.5, 0, 297.5, 842);
            }
            for (int i = 0; i < (firstColumnEntryPoints.Count()/2)-1; i++)
            {
                gfx.DrawLine(pen,
                    0,
                    (i + 1.5) * spaceXunit + (CardHeightMultiplier * (1 + i)) * imageHeightXUnit,
                    595,
                    (i + 1.5) * spaceXunit + (CardHeightMultiplier * (1 + i)) * imageHeightXUnit);
            }
        }

        private static void DrawBorder(double x, double y, double width, double height)
        {
            gfx.DrawRectangle(pen,new XRect(x,y,width, height));
        }
        private static void DrawSideBorder(Point p1, Point p2)
        {
            gfx.DrawLine(pen,p1,p2);
        }
        private static void CheckSchema()
        {
            CheckIfCardsFit();
            CalculateEntryPoint();
        }

        static int cardsInFirstColumn = 0;
        static int cardsInSecondColumn = 0;

        private static void CheckIfCardsFit()
        {
            CheckWidthFit();
            int checkedHeightFit = CheckHeightFit();
            if (WidthFitness == false)
            {
                //One column available with max 4 cards in it
                Settings.CardOnPage = Convert.ToInt32(Settings.CardOnPage) > checkedHeightFit ? checkedHeightFit : Settings.CardOnPage;
                cardsInFirstColumn = Convert.ToInt32(Settings.CardOnPage);
            }
            else
            {
                //Two columns with max column fitness
                Settings.CardOnPage = Convert.ToInt32(Settings.CardOnPage) > 2 * checkedHeightFit ? 2 * checkedHeightFit : Settings.CardOnPage;
                if (Convert.ToInt32(Settings.CardOnPage) > checkedHeightFit)
                {
                    cardsInFirstColumn = checkedHeightFit;
                    cardsInSecondColumn = Convert.ToInt32(Settings.CardOnPage) - checkedHeightFit;
                }
                else
                {
                    cardsInFirstColumn = Convert.ToInt32(Settings.CardOnPage);
                }
            }
        }

        // When cards have 200 pixels height we have 84 additional pixels for spaces above,beetwen and under cards - five in total
        // But in checking height we multiply by 4 so 84/4 = 21 
        // In the ducemnt we have 16 pixels beetwen cards and beetwen card and end of page
        private const int spaceAboveCard = 21;
        private const int spaceBeetweenCards = 15;
        private const int CardHeightMultiplier = 4;
        private const int CardWidthMultiplier = 2;
        private const int HeightInPixels = 1684;
        private const int WidthInPixels = 1191;
        private static int imageHeight;
        private static int imageWidth;
        private static bool WidthFitness = true;

        private static void CheckWidthFit()
        {
            if (Settings.CardOnPage >= 2)
            {
                if (imageWidth * CardWidthMultiplier + spaceBeetweenCards > WidthInPixels)
                {
                    WidthFitness = false;
                }
            }
        }
        private static int CheckHeightFit()
        {
            int cardsInOneColumn = Convert.ToInt32(Math.Ceiling(Settings.CardOnPage / 2));
            int allCardsInColumnHeight = cardsInOneColumn * (imageHeight * CardHeightMultiplier + spaceAboveCard);
            int maxCardsInColumn = 4;

            if (allCardsInColumnHeight > HeightInPixels)
            {
                maxCardsInColumn =
                    Convert.ToInt32(
                        Math.Floor(
                            Convert.ToDouble(HeightInPixels) / (imageHeight * CardHeightMultiplier + spaceAboveCard)
                            ));
                maxCardsInColumn = maxCardsInColumn > 4 ? 4 : maxCardsInColumn;
            }
            return maxCardsInColumn;
        }
        
        static List<double> firstColumnEntryPoints;
        static List<double> secondColumnEntryPoints;

        //In this method we have to divide all pixels values by 2 because
        //Page in Pixels is 1684 x 1191 but in XUnit used by PDFSHARP is 595 x 842
        //It's approximation because one pixels is like 0.50001... Xunits

        private static int imageHeightXUnit;
        private static int imageWidthXUnit;
        private static int entryPointStep;
        private const int spaceXunit = 8;
        private static void CalculateEntryPoint()
        {
            imageHeightXUnit = imageHeight / 2;
            imageWidthXUnit = imageWidth / 2;
            entryPointStep = imageHeightXUnit * 3;

            firstColumnEntryPoints = new List<double>();
            secondColumnEntryPoints = new List<double>();

            if (cardsInFirstColumn > 0)
            {
                CalculateColumnEntryPoints(ref firstColumnEntryPoints, ref cardsInFirstColumn);
            }
            if (cardsInSecondColumn > 0)
            {
                CalculateColumnEntryPoints(ref secondColumnEntryPoints, ref cardsInSecondColumn);
            }
        }
        private static void CalculateColumnEntryPoints(ref List<double> ColumnEntryPoints, ref int cardCount)
        {
            //First Space
            ColumnEntryPoints.Add(spaceXunit);
            // Second image - in other words: entryPoint + entryPointStep, but i dont wanna get value from list
            ColumnEntryPoints.Add(entryPointStep + spaceXunit);

            double entryPoint = entryPointStep + spaceXunit;

            for (int i = 0; i < cardCount * 2 - 2; i++) //One Card has two picture (*2) but we already added first points (-2)
            {
                if (i % 2 == 0) // Space beetwen cards
                {
                    entryPoint += (imageHeightXUnit + spaceXunit);

                }
                else //Space inside a card
                {
                    entryPoint += entryPointStep;
                }
                ColumnEntryPoints.Add(entryPoint);
            }
        }
        private static void DisposeAll()
        {
            Image.Dispose();
            document.Dispose();
            gfx.Dispose();
        }
        private static void ResetProperties()
        {

            DataSet = null;
            firstColumnEntryPoints = null;
            secondColumnEntryPoints = null;
            DataSetIterator = 0;
            imagePath = "";
            imageHeightXUnit = 0;
            imageWidthXUnit = 0;
            entryPointStep = 0;
            pageCount = 0;
            fileName = "BusinessCards";
            imageHeight = 0;
            imageWidth = 0;
        }
    }
}
