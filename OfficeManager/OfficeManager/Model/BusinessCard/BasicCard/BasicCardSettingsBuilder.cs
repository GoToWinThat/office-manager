﻿using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.BusinessCard.BasicCard
{
    public class BasicCardSettingsBuilder
    {
        private readonly BasicCardSettings basicCardSettings = new BasicCardSettings();
        public BasicCardSettingsBuilder(BasicCardSettings basicCardSettings)
            => this.basicCardSettings = basicCardSettings;

        public  BasicCardSettingsBuilder(int cardsOnPage, bool doublePrint, bool splitterLines, bool border, bool sideBorder, string font, double fontSize, string fontColor)
        {
            basicCardSettings.CardOnPage = cardsOnPage;

            basicCardSettings.DoublePrint = doublePrint;

            basicCardSettings.SplitterLines = splitterLines;

            basicCardSettings.Border = border;

            basicCardSettings.SideBorder = sideBorder;

            basicCardSettings.Font = new XFont(font, fontSize);

            basicCardSettings.FontColor = ColorsToBrush[fontColor];
        }

        public BasicCardSettingsBuilder SetCardsOnPage(int cardsOnPage)
        {
            basicCardSettings.CardOnPage = cardsOnPage;
            return this;
        }
        public BasicCardSettingsBuilder SetDoublePrint(bool doublePrint)
        {
            basicCardSettings.DoublePrint = doublePrint;
            return this;
        }
        public BasicCardSettingsBuilder SetSplitingLines(bool splitterLines)
        {
            basicCardSettings.SplitterLines = splitterLines;
            return this;
        }
        public BasicCardSettingsBuilder SetBorder(bool border)
        {
            basicCardSettings.Border = border;
            return this;
        }
        public BasicCardSettingsBuilder SetSideBorder(bool sideBorder)
        {
            basicCardSettings.SideBorder = sideBorder;
            return this;
        }
        public BasicCardSettingsBuilder SetFont(string font, double fontSize)
        {
            basicCardSettings.Font = new XFont(font, fontSize);
            return this;
        }
        public BasicCardSettingsBuilder SetFontColor(string fontColor)
        {
            basicCardSettings.FontColor = ColorsToBrush[fontColor];
            return this;
        }
        public BasicCardSettings GetSettings()
        {
            return this.basicCardSettings;
        }

        public Dictionary<string, XBrush> ColorsToBrush { get; set; } = new Dictionary<string, XBrush>
        {
            { "Black", XBrushes.Black },
            { "Gray", XBrushes.Gray},
            { "Red", XBrushes.Red },
            { "Blue", XBrushes.Blue},
            { "Green", XBrushes.Green },
            { "Purple", XBrushes.Purple},
            { "Yellow", XBrushes.Yellow },
            { "Orange", XBrushes.Orange},
        };
    }
}
