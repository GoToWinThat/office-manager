﻿using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManager.Model.BusinessCard.BasicCard
{
    public class BasicCardSettings
    {
        public double CardOnPage { get; set; }
        public bool SplitterLines { get; set; }
        public bool DoublePrint { get; set; }
        public bool Border { get; set; }
        public bool SideBorder { get; set; }
        public XFont Font { get; set; }
        public XBrush FontColor { get; set; }
    }
}
